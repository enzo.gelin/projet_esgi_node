const fetch = require('node-fetch')
const appConfig = require('../config/app.config')
const bcrypt = require('bcryptjs')

exports.create = async (req, res) => {
    const transaction = {
        cardNumber: req.body.card_number,
        cardDate: req.body.card_date,
        cardCvc: req.body.card_cvc,
        price: req.body.price,
        currency: req.body.currency,
        callback: req.body.callback,
        id: req.body.transaction_id
    };

    res.json({"message": "processing"})

    // Fake process
    await require('timers/promises').setTimeout(10000);

    const payload = JSON.stringify({
        success: true,
        transaction: transaction.id,
        message: "payment confirmed",
    })

    const signature = bcrypt.hashSync(payload, appConfig.API_KEY)

    console.log(payload)
    console.log(signature)

    fetch(transaction.callback, {
        method: 'POST',
        body: payload,
        headers: {
            'x-webhook-signature': signature,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(data => console.log(data))
        .catch(e => console.log(e))
}
