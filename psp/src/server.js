const appConfig = require("./config/app.config.js");
const express = require("express");
const routes = require("./routes");
const auth = require("./middleware/auth.middleware")

const server = express();

server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(auth)
server.use(routes);

server.listen(appConfig.PORT || 4500, () => console.log("Server listening"));
