const appConfig = require('../config/app.config')

module.exports = function (req, res, next) {
    if (req.headers.authorization !== appConfig.API_KEY) {
        res.sendStatus('401')
    } else {
        next();
    }
}