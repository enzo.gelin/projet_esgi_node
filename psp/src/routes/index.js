const router = require('express').Router();
const transaction = require('../controllers/transaction.controller')
const { validate } = require('../validators')
const { paymentTransactionValidator } = require('../validators/transaction.validator')

router.post('/', paymentTransactionValidator(), validate, transaction.create)

module.exports = router;
