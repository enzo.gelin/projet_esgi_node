const { body } = require('express-validator')

const payment = () => {
    return [
        body('card_number').notEmpty().trim().isCreditCard(),
        body('card_date').notEmpty().trim().isLength({min: 5, max: 5}).matches(/^\d\d\/\d\d$/),
        body('card_cvc').notEmpty().trim().isLength({min: 3, max: 3}),
        body('price').notEmpty().trim(),
        body('currency').notEmpty().trim(),
        body('callback').notEmpty(),
        body('transaction_id').notEmpty()
    ]
}

module.exports = {
    paymentTransactionValidator: payment
}
