const express = require("express");
const HttpCodeRouter = require("./routes/HttpCodeRouter");
const BackOffice = require("./routes/BackOffice");
const mustacheExpress = require("mustache-express");

const app = express();
app.engine("mustache", mustacheExpress());
app.set("view engine", "mustache");
app.set("views", __dirname + "/views");

app.use(express.json());
app.use(express.urlencoded());

app.use("/webhook", (req, res) => {
    console.log(req.body)
})

app.use("/dashboard", BackOffice);

app.listen(process.env.PORT || 4500, () => console.log("Server listening"));
