const { Sequelize, Model, DataTypes } = require('lib/sequalize');
const sequelize = new Sequelize('sqlite::memory:');

class Article extends Model { }
Article.init({
    title: DataTypes.STRING,
    content: DataTypes.STRING,
    tags: DataTypes.ARRAY(DataTypes.STRING),
}, { sequelize, modelName: 'article' });

(async () => {
    await sequelize.sync();
    const recipe_1 = await Article.create({
        title: 'couscous de maitre',
        content: 'couscous tagine agneau avec pruneau',
        tags: '["cuisine","orientale","épices"]'
    });
    console.log(recipe_1.toJSON());
})();