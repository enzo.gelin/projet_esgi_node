import React, { useContext, useState } from "react";
import Button from "../lib/Button";
import Modal from "../lib/Modal";
import Form from "./Form";
import { ListContext } from "../../contexts/ListContext";

export default function List() {
  const { list, totalPrice, editElement, deleteElement } = useContext(ListContext);
  const [modal, setModal] = useState(false);

  const onSubmit = (values) => {
      editElement(values);
      setModal(false);
  };
  
  const createTransaction = () => {
    const data = {
      cart: list,
      totalPrice,
      currency: "EUR",
      consumer: {
        lastname: "Foo",
        firstname: "Bart",
      },
      shippingAddress: {
        address: "1 rue Bouvier",
        zipCode: 75011,
        city: "Paris",
        country: "France",
      },
      billingAddress: {
        address: "1 rue Bouvier",
        zipCode: 75011,
        city: "Paris",
        country: "France",
      },
    };

    fetch("http://localhost:3001/transactions", {
      method: "POST",
      headers: {
        Authorization: "BASIC " + localStorage.getItem("credentials"), // base64("username:password")
        "Content-type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => response.json())
      .then((data) => console.log(data));
  };

  return (
    <>
      <table className="w-full text-sm lg:text-base" cellSpacing="0">
        <thead>
          <tr className="h-12 uppercase">
            <th className="text-left">Product</th>
            <th className="lg:text-right text-left pl-5 lg:pl-0">
              <span className="lg:hidden" title="Quantity">Qtd</span>
              <span className="hidden lg:inline">Quantity</span>
            </th>
            <th className="hidden text-right md:table-cell">Unit price</th>
            <th className="text-right">Total price</th>
          </tr>
        </thead>
        <tbody>
          {list.map(item => (
            <tr key={item.id}>
              <td>
                <a href="#">
                  <p className="mb-2 md:ml-4">{item.name}</p>

                </a>
              </td>
              <td className="justify-center md:justify-end md:flex mt-4">
                <div className="w-20 h-10">
                  <div className="relative flex flex-row w-full h-8">
                    <Button title={item.quantity} onClick={() => setModal(item)} />
                  </div>
                </div>
              </td>
              <td className="hidden text-right md:table-cell">
                <span className="text-sm lg:text-base font-medium"> {item.unitPrice} € </span>
              </td>
              <td className="text-right">
                <span className="text-sm lg:text-base font-medium"> {item.unitPrice * item.quantity} € </span>
              </td>
              <td className="text-right">
                <Button title="x" onClick={() => deleteElement(item)} />
              </td>
            </tr>
          ))}

        </tbody>
        <tfoot>
          <tr className="h-12 uppercase" >
            <th className="text-left" colSpan="1">Montant Total</th>
            <th className="text-left" colSpan="2">{totalPrice} €</th>
            <th className="text-right" colSpan="4"><Button title="Passer au paiement" onClick={() => createTransaction()} disabled={list.length === 0} /></th>
          </tr>
        </tfoot>
      </table>

      <Modal
      title="Modifier dans le panier"
      open={modal}
      onClose={() => setModal(false)}
      >
        <Form onSubmit={onSubmit} selectedValue={modal} />
      </Modal>
    </>
  );
}
