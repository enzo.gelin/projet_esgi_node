import React, { useState } from "react";
import Button from "../lib/Button";

export default function Form({ onSubmit, selectedValue }) {
  const [values, setValues] = useState(
    selectedValue === true
      ? {
          name: "",
          quantity: 0,
          unitPrice: 0,
        }
      : selectedValue
  );

  const _onSubmit = (event) => {
    event.preventDefault();
    // Vanilla JS approch
    //const newData = new FormData(event.target);
    //const values = newData.entries.reduce((acc, [key, value]) => {
    //  acc[key] = value;
    //  return acc;
    //}, {});
    if (selectedValue) onSubmit(values);
  };
  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <form onSubmit={_onSubmit} style={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
      <label>{values.name} - {values.unitPrice}€</label>
      <input
        value={values.quantity}
        onChange={handleChange}
        name="quantity"
        type="number"
        style={{border: "1px solid grey", borderRadius: 5, padding: "2px 5px", width: 100}}
      />
      <Button title="Submit" />
    </form>
  );
}
