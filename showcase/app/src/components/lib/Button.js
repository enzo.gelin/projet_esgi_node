import React from "react";

function Button({
  variant = "default",
  title,
  size = 20,
  theme,
  ...restProps
}) {
  const style = {
    color: "#fff",
    backgroundColor: "#000",
    borderRadius: 5
  };

  // if (["rounded", "icon"].includes(variant)) {
  //   style.borderRadius = "50%";
  // }

  // if (variant === "icon") {
  //   style.width = size;
  //   style.height = style.width;
  //   style.maxHeight = style.width;
  //   style.maxWith = style.width;
  //   style.minHeight = style.width;
  //   style.minWith = style.width;
  // }

  return (
    <button className="inline-block no-underline hover:text-black hover:underline py-2 px-4" style={style} {...restProps}>
      {title}
    </button>
  );
}

export default Button;
