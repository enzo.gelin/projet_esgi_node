import React, { useState, useContext } from "react";
import Button from "./lib/Button";
import Modal from "./lib/Modal";
import Form from "./Cart/Form";
import { ListContext } from "../contexts/ListContext";

const data = [
  { id: 1, name: "plate", unitPrice: 1, quantity: 3 },
  { id: 2, name: "spoon", unitPrice: 3, quantity: 3 },
  { id: 3, name: "riz", unitPrice: 2, quantity: 3 },
  { id: 4, name: "pate", unitPrice: 1, quantity: 3 },
  { id: 5, name: "pain", unitPrice: 7, quantity: 3 },
  { id: 6, name: "lentille", unitPrice: 5, quantity: 3 },
  { id: 7, name: "chou", unitPrice: 11, quantity: 3 },
  { id: 8, name: "carotte", unitPrice: 10, quantity: 3 },
];

function ItemShow() {
  const { addElement } = useContext(ListContext);
  
  const [modal, setModal] = useState(false);

  const onSubmit = (values) => {
    addElement(values);
    setModal(false);
  };

  return (
    <>
      {
        data.map(item => (
          <div className="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col" key={item.id}>
            <a href="#">
              <img className="hover:grow hover:shadow-lg" src="https://images.unsplash.com/photo-1555982105-d25af4182e4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&h=400&q=80" />
              <div className="pt-3 flex items-center justify-between">
                <p className="">{item.name}</p>
                <p className="pt-1 text-gray-900">{item.unitPrice} €</p>
                <Button title="+" onClick={() => setModal(item)} />
              </div>

            </a>
          </div>
        ))
      }

      <Modal
      title="Ajouter au panier"
      open={modal}
      onClose={() => setModal(false)}
      >
        <Form onSubmit={onSubmit} selectedValue={modal} />
      </Modal></>

  );
}

export default ItemShow;
