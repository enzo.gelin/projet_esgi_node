import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Page as Store } from "./components/Page";
import { Page1 as Panier } from "./components/Page1";
import Header from "./components/Header";
import Footer from "./components/Footer";
import ListProvider from "./contexts/ListContext";

function App() {
  return (
    <div className="App bg-white text-gray-600 work-sans leading-normal text-base tracking-normal">
      <Router>
        <ListProvider>  
          <Header />

          <Switch>

              {/* Store */}
              <Route exact path='/'>
                  <Store/>
              </Route>

              {/* Store */}
              <Route exact path='/panier'>
                  <Panier/>
              </Route>

          </Switch>
        </ListProvider>
      </Router>
      
      <Footer />
    </div>
  );
}

export default App;
