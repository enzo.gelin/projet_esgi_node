const appConfig = require('../config/app.config')
const jwt = require("jsonwebtoken");

exports.appUrl = (path) => {
    return `${appConfig.APP_URL}/${path}`
}

exports.apiUrl = (path) => {
    return `${appConfig.API.URL}/${path}`
}

exports.apiHost = (path) => {
    return `${appConfig.API.HOST}/${path}`
}

exports.storage = (path) => {
    return `${__dirname}/../storage/${path}`
}

exports.uploads = (path) => {
    return `${__dirname}/../storage/uploads/${path}`
}

exports.views = (path) => {
    return `${__dirname}/../views/${path ?? ''}`
}

exports.random = (min, max) => {
    return Math.floor(
        Math.random() * (max - min + 1) + min
    )
}

exports.authMiddleware = (req, res, next) => {
    try {
        console.log(req)
        const token = req.header("x-auth-token");
        if (!token) return res.status(403).send("Access denied.");

        const decoded = jwt.verify(token, process.env.JWTPRIVATEKEY);
        req.user = decoded;
        next();
    } catch (error) {
        res.status(400).send("Invalid token");
    }
};
