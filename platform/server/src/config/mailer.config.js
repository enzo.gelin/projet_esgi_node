module.exports = {
    HOST: process.env.MAILER_HOST,
    PORT: process.env.MAILER_PORT,
};