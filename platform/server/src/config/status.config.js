module.exports = {
    user: {
        waiting: 0,
        active: 1,
        incomplete: 2,
        refused: 3
    },
    transaction: {
        default: 0,
        validate: 1,
        rejected: 2
    }
}