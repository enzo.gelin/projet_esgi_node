module.exports = {
    APP_KEY: process.env.APP_KEY,
    PORT: process.env.PORT,
    APP_URL: process.env.APP_URL,
    API: {
        URL: process.env.API_URL,
        HOST: process.env.API_HOST,
    },
    PSP: {
        URL: process.env.PSP_URL,
        HOST: process.env.PSP_HOST,
        SECRET: process.env.PSP_SECRET
    }
};