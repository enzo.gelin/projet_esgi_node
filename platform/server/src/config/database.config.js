module.exports = {
    postgres: {
        URL: process.env.DATABASE_URL,
        dialect: "postgres",
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    },
    mongo: {
        URL: process.env.MONGO_URL,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        authSource: "admin",
    }
};