const { Schema } = require("mongoose");
const connection = require("./index");

const TransactionSchema = new Schema({
    user_id: Number,
    lastname: String,
    firstname: String,
    email: String,
    address: String,
    delivery: String,
    cart: Object,
    price: Number,
    currency: String
}, {
    timestamps: true
});

const Transaction = connection.model("Transaction", TransactionSchema);

module.exports = Transaction;
