const mongoose = require("mongoose");
const { mongo } = require("../../config/database.config")

mongoose
    .connect(mongo.URL, {
        useNewUrlParser: mongo.useNewUrlParser,
        useUnifiedTopology: mongo.useUnifiedTopology,
        authSource: mongo.authSource,
    })
    .then((_) => console.log("mongo connected"));

module.exports = mongoose.connection;
