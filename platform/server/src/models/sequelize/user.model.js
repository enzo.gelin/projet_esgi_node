const bcrypt = require('bcryptjs');
const appConfig = require("../../config/app.config");
const welcomeEmail = require('../../notifications/welcome.notifications')
const statusEmail = require('../../notifications/status.notifications')
const { Model, DataTypes } = require("sequelize");

module.exports = (connection) => {
    const PROTECTED_ATTRIBUTES = ['password']

    class User extends Model {
        toJSON () {
            let attributes = Object.assign({}, this.get())
            for (let a of PROTECTED_ATTRIBUTES) {
                delete attributes[a]
            }
            return attributes
        }
    }

    User.init(
        {
            firstname: {
                type: DataTypes.STRING,
                allowNull: false
            },
            lastname: {
                type: DataTypes.STRING,
                allowNull: false
            },
            email: {
                type: DataTypes.STRING,
                validate: {
                    isEmail: true,
                },
                allowNull: false,
                unique: true
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false
            },
            status: {
                type: DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
            },
            company: {
                type: DataTypes.STRING,
                allowNull: false
            },
            kbis: {
                type: DataTypes.STRING,
                allowNull: false
            },
            redirect_success: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            redirect_failed: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            webhook: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            is_admin: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
                allowNull: false
            },
            client_id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                unique: true
            },
            secret_id: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                unique: true
            },
        },{
            sequelize: connection,
            modelName: "user",
        }
    );


    const encodePassword = async (user) => {
        if (user.changed('password')) {
            user.password = await bcrypt.hash(user.password, appConfig.APP_KEY);
        }
    };

    User.addHook("beforeCreate", encodePassword);
    User.addHook("beforeUpdate", encodePassword);
    User.addHook("afterCreate", welcomeEmail.send);
    User.addHook("afterUpdate", statusEmail.send);

    return User;
};
