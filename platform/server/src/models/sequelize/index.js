const { postgres } = require("../../config/database.config.js");
const User = require("./user.model.js")

const Sequelize = require("sequelize");

const sequelize = new Sequelize(postgres.URL, {
    dialect: postgres.dialect,
    operatorsAliases: 0,

    pool: {
        max: postgres.pool.max,
        min: postgres.pool.min,
        acquire: postgres.pool.acquire,
        idle: postgres.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = User(sequelize);

module.exports = db;
