const { body } = require('express-validator')
const db = require("../models/sequelize");
const User = db.users;

const create = () => {
    return [
        body('client_id').notEmpty(),
        body('secret_id').notEmpty(),
        body('lastname').notEmpty(),
        body('firstname').notEmpty(),
        body('email').isEmail(),
        body('address').notEmpty(),
        body('delivery').notEmpty(),
        body('cart').notEmpty(),
        body('price').notEmpty(),
        body('currency').notEmpty()
    ]
}

const payment = () => {
    return [
        body('card_number').notEmpty().trim().isCreditCard(),
        body('card_date').notEmpty().trim().isLength({min: 5, max: 5}).matches(/^\d\d\/\d\d$/),
        body('card_cvc').notEmpty().trim().isLength({min: 3, max: 3})
    ]
}

module.exports = {
    createTransactionValidator: create,
    paymentTransactionValidator: payment
}