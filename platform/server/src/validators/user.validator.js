const { body } = require('express-validator')
const { user: userStatus } = require('../config/status.config')
const db = require("../models/sequelize");
const User = db.users;

const create = () => {
    return [
        body('firstname').notEmpty(),
        body('lastname').notEmpty(),
        body('email').isEmail().normalizeEmail().custom(value => {
            return User.findOne({ where: {email: value}}).then(user => {
                if (user) {
                    return Promise.reject('E-mail already in use');
                }
            });
        }),
        body('password').isLength({ min: 8 }),
        body('passwordConfirmation').exists().custom((value, { req }) => value === req.body.password),
        body('company').notEmpty(),
        body('redirect_success').notEmpty(),
        body('redirect_failed').notEmpty(),
        body('webhook').notEmpty(),
        body('is_admin').notEmpty().isBoolean()
    ]
}

const update = () => {
    return [
        body('firstname').notEmpty(),
        body('lastname').notEmpty(),
        body('email').isEmail().normalizeEmail(),
        body('company').notEmpty(),
        body('redirect_success').notEmpty(),
        body('redirect_failed').notEmpty()
    ]
}

const status = () => {
    return [
        body('status').isIn(Object.values(userStatus))
    ]
}

module.exports = {
    createUserValidator: create,
    updateUserValidator: update,
    statusUserValidator: status,
}
