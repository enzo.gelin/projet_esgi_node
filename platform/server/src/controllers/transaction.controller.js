const Transaction = require('../models/mongo/transaction.model')
const getSymbolFromCurrency = require('currency-symbol-map')
const db = require("../models/sequelize");
const User = db.users;
const fetch = require("node-fetch");
const { apiHost, apiUrl } = require("../helpers")
const dayjs = require('dayjs')
const { PSP } = require('../config/app.config')
const { transaction: statusTransaction } = require('../config/status.config')

exports.create = (req, res) => {
    const transaction = {
        lastname: req.body.lastname,
        firstname: req.body.firstname,
        email: req.body.email,
        address: req.body.address,
        delivery: req.body.delivery,
        cart: req.body.cart,
        price: req.body.price,
        currency: req.body.currency
    };

    User.findOne({ where: { client_id: req.body.client_id, secret_id: req.body.secret_id }})
        .then(data => {
            transaction.user_id = data.id

            Transaction.create(transaction)
                .then(data => res.send({url: apiUrl(`checkout/${data._id}`)}))
                .catch(err => res.status(500).send({ message: err.message || "Some error occurred while creating the User."}))
        })
        .catch(err => res.status(500).send({message: "Invalid Credentials"}));
};

exports.show = (req, res) => {
    Transaction.findById(req.params.id)
        .then(data => {
            if (dayjs() > dayjs(data.createdAt).add(20, 'minute') || data.status !== statusTransaction.default) {
                res.render('expired')
            } else {
                res.render('checkout', {
                    'price': data.price,
                    'currency': getSymbolFromCurrency(data.currency)
                })
            }
        })
        .catch(err => res.status(500).send({message: "Invalid Credentials"}));
}

exports.payment = (req, res) => {
    Transaction.findById(req.params.id)
        .then(transaction => {
            User.findByPk(transaction.user_id)
                .then(user => {
                    const body = {
                        card_number: req.body.card_number,
                        card_date: req.body.card_date,
                        card_cvc: req.body.card_cvc,
                        price: transaction.price,
                        currency: transaction.currency,
                        transaction_id: transaction._id,
                        callback: apiHost(`webhook`)
                    }

                    fetch(PSP.HOST, {
                        method: 'post',
                        headers: new fetch.Headers({
                            'Authorization': PSP.SECRET,
                            'Content-Type': 'application/json; charset=UTF-8'
                        }),
                        body: JSON.stringify(body)
                    })
                        .then(data => data.json())
                        .then(data => {
                            if (data.message === "processing") {
                                res.redirect(user.redirect_success)
                            } else {
                                res.redirect(user.redirect_failed)
                            }
                        })
                        .catch(e => console.log(e))
                })
        })
}
