const { PSP } = require('../config/app.config')
const bcrypt = require('bcryptjs')
const Transaction = require("../models/mongo/transaction.model");
const db = require("../models/sequelize");
const User = db.users;

exports.listen = (req, res) => {
    const signature = req.headers["x-webhook-signature"];

    const payload = JSON.stringify(req.body);

    const secure = bcrypt.compareSync(payload, signature)

    console.log(req.body)

    if (secure) {
        Transaction.findById(req.body.transaction)
            .then(transaction => {
                User.findByPk(transaction.user_id)
                    .then(user => {

                        const payload = JSON.stringify({
                            success: true,
                            transaction: transaction._id,
                            message: "payment confirmed",
                        })

                        const signature = bcrypt.hashSync(payload, user.secret_id)

                        fetch(user.webhook, {
                            method: 'POST',
                            body: payload,
                            headers: {
                                'x-webhook-signature': signature,
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            }
                        })
                            .then(data => res.send("Webhook delivred"))
                            .catch(e =>
                                res.send("A error has occured!"))

                    })
            })

    } else {
        res.status(401).send("Not Authorized")
    }
}
