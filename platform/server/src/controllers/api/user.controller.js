const db = require("../../models/sequelize");
const User = db.users;
const Op = db.Sequelize.Op;
const mime = require('mime-types')
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const {v4: uuidv4} = require('uuid')
const {uploads} = require('../../helpers')

const accessTokenSecret = 'youraccesstokensecret';
const refreshTokenSecret = 'youraccesstokensecret';
let refreshTokens = [];

exports.create = (req, res) => {
    if (!req.files) {
        res.send({
            status: false,
            message: 'No file uploaded'
        });
    }

    const kbis = req.files.kbis;
    const filename = `${uuidv4()}.${mime.extension(kbis.mimetype)}`;

    kbis.mv(uploads(filename))

    var salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(req.body.password, salt);

    const user = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hash,
        company: req.body.company,
        is_admin: req.body.is_admin,
        kbis: filename,
        redirect_success: req.body.redirect_success,
        redirect_failed: req.body.redirect_failed,
        webhook: req.body.webhook,
    };

    User.create(user)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });
};

exports.register = (req, res) => {
    if (!req.files) {
        res.send({
            status: false,
            message: 'No file uploaded'
        });
    }

    const kbis = req.files.kbis;
    const filename = `${uuidv4()}.${mime.extension(kbis.mimetype)}`;

    kbis.mv(uploads(filename))

    const hash = bcrypt.hashSync(req.body.password, "");

    const user = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hash,
        company: req.body.company,
        is_admin: false,
        kbis: filename,
        redirect_success: "",
        redirect_failed: "",
    };

    User.create(user)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });
};

exports.token = (req, res) => {
    const {token, refresh} = req.body;

    if (!token) {
        return res.sendStatus(401);
    }

    if (!refreshTokens.includes(refresh)) {
        return res.sendStatus(403);
    }

    jwt.verify(token, refreshTokenSecret, (err, user) => {
        if (err) {
            return res.sendStatus(403);
        }

        res.json({
            verify: true
        });
    });
}

exports.findAll = (req, res) => {
    const limit = req.query.limit ?? 20
    const page = req.query.page ?? 1
    const offset = page * limit - limit

    User.findAndCountAll({
        limit: limit,
        offset: offset,
        attributes : ["id", "firstname", "lastname", "email","company","status","redirect_success", "redirect_failed"]
    })
        .then((data) => {
            const pagination = {
                data: data.rows,
                page: req.body.page,
                total: data.count
            }
            console.log(data)
            res.json(pagination)
        })
        .catch((e) => {
            res.sendStatus(500)
        });
};

exports.findNotActive = (req, res) => {
    User.findAll({
        where: {
            status: {[Op.not]: 1}
        }
    })
        .then((data) => res.json(data))
        .catch((e) => res.sendStatus(500));
};

exports.findOne = (req, res) => {
    User.findByPk(req.params.id)
        .then((data) =>
            data === null ? res.sendStatus(404) : res.json(data)
        )
        .catch((e) => res.sendStatus(500));
};

exports.login = (req, res) => {
    User.findOne({where: {email: req.body.email, is_admin: true}})
        .then((data) => {
                if (data !== null) {
                    bcrypt.compare(req.body.password, data.password, (err, success) => {
                        if (err) throw err
                        if (success) {
                            console.log(data)
                            const user = {email: data.email,client_id: data.client_id, secret_id: data.secret_id, id: data.id, is_admin: data.is_admin}
                            const accessToken = jwt.sign(user, accessTokenSecret);
                            const refreshToken = jwt.sign(user, refreshTokenSecret);

                            refreshTokens.push(refreshToken);

                            res.json({
                                accessToken,
                                refreshToken,
                                user: {email: data.email,client_id: data.client_id, secret_id: data.secret_id, id: data.id, is_admin: data.is_admin}
                            });

                        } else {
                            return res.status(401).json({ msg: "Invalid credencial" })
                        }
                    })
                } else {
                    res.sendStatus(400)
                }
            }
        )
        .catch((e) => {
            console.log(e)
            res.sendStatus(500).json
        });
};

exports.update = (req, res) => {
    User.update(req.body, {
        where: {id: req.params.id},
        returning: true,
        individualHooks: true,
    })
        .then(([, [data]]) =>
            data === undefined ? res.sendStatus(404) : res.json(data)
        )
        .catch((e) => {
            if (e.name === "SequelizeValidationError") {
                res.status(400).json(e);
            } else {
                console.log(e);
                res.sendStatus(500);
            }
        });
};

exports.delete = (req, res) => {
    User.destroy({where: {id: req.params.id}})
        .then((data) =>
            data === 0 ? res.sendStatus(404) : res.sendStatus(204)
        )
        .catch((e) => res.sendStatus(500));
};

exports.status = (req, res) => {
    User.findByPk(req.params.id)
        .then((data) => User.update({status: req.body.status}, {
            where: {id: req.params.id},
            individualHooks: true
        }))
        .then((data) => res.json({message: "User confirmed"}))
        .catch((e) => res.sendStatus(500));
}
