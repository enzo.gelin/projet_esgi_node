const Transaction = require('../../models/mongo/transaction.model')

exports.findAll = (req, res) => {
    const limit = req.query.limit ?? 20
    const page = req.query.page ?? 1
    const offset = page * limit - limit

    Transaction.find().limit(limit).skip(offset)
        .then((data) => {
            const pagination = {
                data: data.rows,
                page: req.body.page,
                total: data.count
            }
            console.log(data)
            res.json(pagination)
        })
        .catch((e) => {
            console.log(e)
            res.sendStatus(500)
        });
};

exports.findOne = (req, res) => {
    Transaction.find(req.params.id)
        .then((data) =>
            data === null ? res.sendStatus(404) : res.json(data)
        )
        .catch((e) => res.sendStatus(500));
};

exports.update = (req, res) => {
    Transaction.update(req.body, {
        where: { id: req.params.id },
        returning: true,
        individualHooks: true,
    })
        .then(([, [data]]) =>
            data === undefined ? res.sendStatus(404) : res.json(data)
        )
        .catch((e) => {
            if (e.name === "SequelizeValidationError") {
                res.status(400).json(e);
            } else {
                console.log(e);
                res.sendStatus(500);
            }
        });
};

exports.delete = (req, res) => {
    Transaction.destroy({ where: { id: req.params.id } })
        .then((data) =>
            data === 0 ? res.sendStatus(404) : res.sendStatus(204)
        )
        .catch((e) => res.sendStatus(500));
};
