const {login, register, token} =  require("../../controllers/api/user.controller");
const express = require("express");
const router = express.Router();
const users = require('./user.routes')
const transactions = require('./transaction.routes')
const {authMiddleware} = require("../../helpers");

const server = express();

router.post('/register', register);
router.post('/login', login);
router.post('/token', token)

router.use('/user', users);
router.use('/transactions', transactions);

module.exports = router;
