const router = require('express').Router();
const { validate } = require('../../validators')
const transactionsApi = require('../../controllers/api/transaction.controller')
const transactions = require('../../controllers/transaction.controller')
const { createTransactionValidator } = require('../../validators/transaction.validator')

router.post("/", createTransactionValidator(), validate, transactions.create);
router.get("/:id", transactionsApi.findOne);
router.put("/:id", transactionsApi.update);
router.delete("/:id", transactionsApi.delete);
router.get("/", transactionsApi.findAll);

module.exports = router;
