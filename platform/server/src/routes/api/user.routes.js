const router = require('express').Router();
const { validate } = require('../../validators')
const { createUserValidator, updateUserValidator, statusUserValidator} = require('../../validators/user.validator.js')
const users = require("../../controllers/api/user.controller.js");

router.post("/", createUserValidator(), validate, users.create);

router.get("/", users.findAll);
router.get("/:id", users.findOne);
router.get("/not-active", users.findNotActive);

router.put("/status/:id", statusUserValidator(), validate, users.status);
router.put("/:id", updateUserValidator(), validate, users.update);

router.delete("/:id", users.delete);

module.exports = router;
