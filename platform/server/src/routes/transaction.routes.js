const router = require('express').Router();
const { validate } = require('../validators')
const transactions = require('../controllers/transaction.controller')
const { createTransactionValidator, paymentTransactionValidator } = require('../validators/transaction.validator')

router.post("/", createTransactionValidator(), validate, transactions.create);
router.post("/:id", paymentTransactionValidator(), validate, transactions.payment);
router.get("/:id", transactions.show);

module.exports = router;
