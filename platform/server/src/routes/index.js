const router = require('express').Router();
const api = require('./api')
const transaction = require('./transaction.routes')
const webhook = require("./webhook.routes");

router.use('/api', api);
router.use('/checkout', transaction)
router.use("/webhook", webhook);

module.exports = router;
