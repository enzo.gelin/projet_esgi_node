const appConfig = require("./config/app.config.js")
const express = require("express")
const routes = require("./routes")
const db = require("./models/sequelize")
const { views } = require("./helpers")
const bcrypt = require("bcryptjs")
const cors = require('cors')
const fileUpload = require('express-fileupload')
const {PSP} = require("./config/app.config")

const server = express();

server.use(cors())
server.engine('html', require('hbs').__express);
server.set("view engine", "hbs");
server.set("views", views());

server.use(express.json());

server.use(fileUpload({createParentPath: true}));
server.use(routes);

db.sequelize.sync({ alter: true });

server.listen(appConfig.PORT || 4500, () => console.log("Server listening"));
