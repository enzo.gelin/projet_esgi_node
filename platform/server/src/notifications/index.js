const mailerConfig = require('../config/mailer.config')
const nodemailer = require("nodemailer");
const hbs = require('nodemailer-express-handlebars');
const { views } = require('../helpers')


const options = {
    viewEngine: {
        extName: '.hbs',
        layoutsDir: views('emails'),
        defaultLayout: '',
    },
    viewPath: views('emails'),
    extName: '.hbs',
}

const transporter = nodemailer.createTransport({
    host: mailerConfig.HOST,
    port: mailerConfig.PORT
});

transporter.use('compile', hbs(options));

module.exports = {
    transporter
}
