const {transporter} = require('./index')

exports.send = (user) => {
    transporter.sendMail({
        from: "Platform <platform@esgi.com>",
        to: user.email,
        subject: "Welcome to Platform!",
        template: "welcome",
        context: {
            firstname: user.firstname,
            lastname: user.lastname,
        }
    });
}