const { transporter } = require('./index')
const { user: userStatus } = require('../config/status.config')

const active = (user) => {
    transporter.sendMail({
        from: "Platform <platform@esgi.com>",
        to: user.email,
        subject: "Your Platform account is ready!",
        template: "active",
        context: {
            firstname: user.firstname,
            lastname: user.lastname,
        }
    });
}

const refused = (user) => {
    transporter.sendMail({
        from: "Platform <platform@esgi.com>",
        to: user.email,
        subject: "Your Platform has been refused",
        template: "refused",
        context: {
            firstname: user.firstname,
            lastname: user.lastname,
        }
    });
}

const incomplete = (user) => {
    transporter.sendMail({
        from: "Platform <platform@esgi.com>",
        to: user.email,
        subject: "Your Platform account is incomplete!",
        template: "incomplete",
        context: {
            firstname: user.firstname,
            lastname: user.lastname,
        }
    });
}

exports.send = (user) => {
    if (user.changed().includes('status') && user.previous().status !== Number(user.status)) {
        switch (Number(user.status)) {
            case userStatus.active:
                active(user)
                break;
            case userStatus.refused:
                refused(user)
                break;
            case userStatus.incomplete:
                incomplete(user)
                break;
        }
    }
}