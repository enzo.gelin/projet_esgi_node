import Login from "./Views/Login";
import Admin from "./Admin";
import {useStoreRehydrated} from "easy-peasy";
import {Redirect, Route, Switch} from "react-router-dom";
import Unauthorized from "./Helper/Unauthorized";
import Register from "./Views/Register";

function WaitForStateRehydration({children}) {
    const isRehydrated = useStoreRehydrated();
    return isRehydrated ? children : null;
}

const Router = () => {
    return (
        <WaitForStateRehydration>
            <Switch>
                <Route exact={true} component={Login} path={"/login"}/>
                <Route exact={true} component={Register} path={"/register"}/>
                <Route exact path='/dashboard/unauthorized' component={Unauthorized}/>
                <Admin/>
            <Redirect to="/login" />
            </Switch>
        </WaitForStateRehydration>
    )
}

export default Router;
