import {Fragment} from "react";
import Dashboard from "./Views/Dashboard";
import ProtectedRoute from "./Helper/ProtectedRoute";
import User from "./Views/User";
import Transaction from "./Views/Transaction";

const Admin = () => {
    return (
        <Fragment>
            <ProtectedRoute exact={true} path={"/dashboard"} component={Dashboard}/>
            <ProtectedRoute exact={true}  path={"/dashboard/transaction"} component={Transaction}/>
            <ProtectedRoute exact={true}  path={"/dashboard/user"} component={User}/>
        </Fragment>
    )
}

export default Admin
