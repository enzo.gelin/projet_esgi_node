import axios from "axios";
import {getHeaders, parseParameters, url} from "./Api";

export async function getUsers(data, user) {
    return await axios.get(
        `${url}user${parseParameters(data)}`,
        {headers: getHeaders(user)}
    ).catch(e => {
        console.log(e.status, e.statusText)
        return null;
    })
}

export async function getUser(data, user) {
    return await axios.get(
        `${url}user/${data}`,
        {headers: getHeaders(user)}
    ).catch(e => e)
}

export async function createUser(data, user) {
    return await axios.post(
        `${url}user`,
        data,
        {headers: getHeaders(user)}
    ).catch(e => e)
}

export async function patchUser(data,id, user) {
    return await axios.put(
        `${url}user/${id}`,
        data,
        {headers: getHeaders(user)}
    ).catch(e => e)
}

export async function deleteUsers(data, user) {
    return await axios.delete(
        `${url}user/${data}`,
        {headers: getHeaders(user)}
    ).catch(e => e)
}
