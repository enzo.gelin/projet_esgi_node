import axios from "axios";
import {getHeaders, parseParameters, url} from "./Api";

export async function getTransactions(data, user) {
    return await axios.get(
        `${url}transactions${parseParameters(data)}`,
        {headers: getHeaders(user)}
    ).catch(e => e)
}

export async function getTransaction(data,user) {
    return await axios.get(
        `${url}transactions/${data}`,
        {headers: getHeaders(user)}
    ).catch(e => e)
}

export async function createTransaction(data, user) {
    return await axios.post(
        `${url}transactions`,
        JSON.stringify(data),
        {headers: getHeaders(user)}
    ).catch(e => e)
}

export async function patchTransaction(data, user) {
    return await axios.patch(
        `${url}transactions/${data.id}`,
        JSON.stringify(data),
        {headers: getHeaders(user)}
    ).catch(e => e)
}

export async function deleteTransactions(data, user) {
    return await axios.delete(
        `${url}transactions/${data}`,
        {headers: getHeaders(user)}
    ).catch(e => e)
}
