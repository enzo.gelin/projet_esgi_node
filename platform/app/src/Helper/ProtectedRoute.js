import React from 'react';
import {Route} from 'react-router-dom';
import {useStoreActions, useStoreState} from "easy-peasy";
import {Redirect} from "react-router";
import {validToken} from "./Api";

const ProtectedRoute = ({component: Component, ...rest}) => {

    const user = useStoreState(state => state.user.user)
    const updateUser = useStoreActions(actions => actions.user.udpate)
    const deleteUser = useStoreActions(actions => actions.user.delete)

    const tokenIsValid = () => {
        console.log(user)
        if (user.access === "") return false;

        return validToken({token: user.access, refresh: user.refresh}).then((req) => {
            console.log(req)
            const res = req.data
            return res.verify;
        }).catch(() => {
            deleteUser()
            return false;
        })
    }

    return (
        <Route {...rest} render={
            props => {
                if (user !== null && tokenIsValid()) {
                    return <Component {...rest} {...props} />
                } else {
                    return <Redirect to={"/login"}/>
                }
            }
        }/>
    )
}

export default ProtectedRoute;
