import axios from "axios";

export const url = 'http://localhost:4500/api/'

export const parseParameters = (obj) => "?"+Object.entries(obj).map(([key, val]) => `${key}=${val}`).join('&')

export const getHeaders = (user) => {

    let headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Content-Type': 'application/json',
    }

    if (user) {
        const access = user.access
        if(access) headers.Authorization = 'Bearer ' + access
    }

    return headers
}

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});

export async function register(data) {
    return await axios.post(
        `${url}register`,
        JSON.stringify(data),
        {headers: getHeaders(false)}
    ).catch(e => e)
}

export async function login(data) {
    return await axios.post(
        `${url}login`,
        JSON.stringify(data),
        {headers: getHeaders(false)}
    ).catch(e => e)
}

export async function validToken(data) {
    return await axios.post(
        `${url}token`,
        JSON.stringify(data),
        {headers: getHeaders(false)}
    ).catch(e => e)
}


