import {action} from "easy-peasy";

export const user = {
    user: {
        access: "",
        refresh: "",
        id: "",
        client_id: "",
        secret_id: '',
        email: "a@a.com",
        isAdmin: true,
    },
    add: action((state, payload) => {
        state.user = payload
    }),
    update: action((state, payload) => {
        return {
            ...state,
            user: [...state.user, payload],
        };
    }),
    delete: action((state, payload) => {
        state.user = null
    })
}
