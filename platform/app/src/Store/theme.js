import {action} from "easy-peasy";

export const theme = {
    theme: "light",
    update: action((state, payload) => {
        return {
            ...state,
            theme: payload,
        };
    }),
}
