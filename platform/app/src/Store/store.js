import {createStore, persist} from "easy-peasy";
import {user} from "./user";

export const store = createStore(
    persist({
        user: user
    }),
    {
        version: 1
    },
);