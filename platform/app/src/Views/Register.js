import React, {useState} from "react";
import {Link} from "react-router-dom";
import {useStoreActions, useStoreState} from "easy-peasy";

import {Redirect} from "react-router";
import {login} from "../Helper/Api";
import {createUser, patchUser} from "../Helper/User.api";
import CustomSelect from "../Component/CustomSelect";

export default function Register() {

    const [values, setValues] = useState(
         {
                firstname: `a`,
                lastname: `a`,
                email: `a@a.fr`,
                company: `a`,
                password: `root`,
            }
    );

    let files = null;

    const _onSubmit = async (event) => {
        event.preventDefault();
        // Vanilla JS approch
        const newData = new FormData(event.target);
        Object.keys(values).forEach(key => newData.append(key, values[key]))
        newData.append("kbis", files)
        let req = await createUser(newData)
    };

    const onChangeFile =event=>{
        files = event.target.files[0]
    }

    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value,
        });
    };

    const changeSelect = (value) => {
        values.status = value
        setValues(values)
    }
    return (
        <section>
            <form onSubmit={_onSubmit}
                  className="flex flex-col w-full p-10 px-8 pt-6 mx-auto my-6 mb-4 bg-white border rounded-lg lg:w-1/2 ">
                <div className="relative pt-4">
                    <label htmlFor="name" className="text-base leading-7 text-blueGray-500">FirstName</label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.firstname} onChange={handleChange} name="firstname" type={"text"}/>
                </div>
                <div className="relative mt-4">
                    <label htmlFor="name" className="text-base leading-7 text-blueGray-500">LastName</label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.lastname} onChange={handleChange} name="lastname" type={"text"}/>
                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Email </label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.email} onChange={handleChange} name="email" type={"email"}/>
                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Company </label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.company} onChange={handleChange} name="company"/>

                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Password </label>
                    <input type="password"
                           className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                           value={values.password} onChange={handleChange} name="password"/>

                </div>
                <section className="flex flex-col w-full h-full p-1 overflow-auto">
                    <header
                        className="flex flex-col items-center justify-center py-12 text-base text-blueGray-500 transition duration-500 ease-in-out transform bg-white border border-dashed rounded-lg focus:border-blue-500 focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2">
                        <p className="flex flex-wrap justify-center mb-3 text-base leading-7 text-blueGray-500">
                            <span>Drag and drop your</span> <span>files anywhere or</span>
                        </p>
                        <button
                            className="w-auto px-2 py-1 my-2 mr-2 text-blueGray-500 transition duration-500 ease-in-out transform border rounded-md hover:text-blueGray-600 text-md border-2 border-gray-200 focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blueGray-100"
                            data-dashlane-rid="a9061af386a605d6" data-form-type="other" data-dashlane-label="true"> Upload a file
                        </button>
                    </header>
                </section>
                <div className="flex items-center w-full pt-4">
                    <button
                        className="w-full py-3 text-base text-white transition duration-500 ease-in-out transform bg-blue-600 border-blue-600 rounded-md border-2 border-black focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blue-800 "
                        data-dashlane-rid="33175fb5453a4c0f" data-form-type="other" data-dashlane-label="true"> Valider
                    </button>
                </div>
            </form>
        </section>
    );
}
