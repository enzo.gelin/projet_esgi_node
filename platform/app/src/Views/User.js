import Modal from "../Component/Modal";
import {useEffect, useState} from "react";
import UserList from "../Component/User/UserList";
import UserShow from "../Component/User/UserShow";
import UserForm from "../Component/User/UserForm";
import {deleteUsers, getUser, getUsers} from "../Helper/User.api";
import {useStoreState} from "easy-peasy";

const User = ({theme}) => {
    const [modal, setModal] = useState(false);
    const [modalShow, setModalShow] = useState(false);
    const [users, setUsers] = useState([])
    const [user, setUser] = useState([])
    const [currentUser, setCurrentUser] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [totalPage, setTotalPage] = useState(50)
    const [itemPerPage, setItemPerPage] = useState(10)
    const [pagination, setPagination] = useState({
        currentPage: currentPage,
        totalPage: itemPerPage
    })
    const admin = useStoreState(state => state.user.user)

    const getData = async () => {
        const table = await getUsers({
            page: currentPage,
            limit: 20
        }, admin)

        setUsers(table.data.data)
        setPagination({
            currentPage: currentPage,
            totalPage: table.data.total / 20
        })
    }

    useEffect(() => {
        getData()
    }, [currentPage]);

    const openModal = () => {
        setUser(null)
        setModal(!modal)
    }

    const openShowModal = async (user) => {
        console.log(user)
        if (user !== false) {
            const userData = await getUser(user.id, admin)
            setUser(userData.data)
        } else {
            setUser(null)
        }
        setModalShow(!modalShow)
    }

    const edit = async (user) => {
        if (user !== false) {
            const userData = await getUser(user.id, admin)
            setUser(userData.data)
        } else {
            setUser(null)
        }
        setModal(!modal)
    }

    const deleteUser = async (id) => {
        const req = await deleteUsers(id, admin)
        if (req.status === 204) {
            const data = users.filter((user) => {
                return user.id !== id
            })
            setUsers(data)
        }
    }

    return (
        <section>
            <h1>Transaction</h1>
            <UserList theme={theme}
                      users={users}
                      pagination={pagination}
                      changePage={setCurrentPage}
                      openModal={openModal}
                      openShowModal={openShowModal}
                      edit={edit}
                      deleteUser={deleteUser}
            />
            <Modal
                theme={theme}
                title="ma modal"
                open={modalShow}
                onClose={() => setModalShow(false)}
            >
                <UserShow theme={theme} deleteUser={deleteUser} user={user}/>
            </Modal>
            <Modal
                theme={theme}
                title={user ? "Edit User" : "Ajout user"}
                open={modal}
                onClose={() => setModal(false)}
            >
                <UserForm selectedValue={user} addUser={getData} closeModal={setModal}/>
            </Modal>
        </section>
    )
}

export default User
