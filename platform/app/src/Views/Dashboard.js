const Dashboard = () => {
    return (
        <section>
            <div className="container px-5 py-24 mx-auto">
                <div className="flex flex-col text-center w-full mb-20">
                    <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Dashboard</h1>
                </div>
                <div className="flex flex-row flex-wrap -m-4 text-center">
                    <div className="p-4 xs:w-full md:w-1/4 sm:w-1/2">
                        <div className="border-2 border-gray-200 px-4 py-6 rounded-lg">
                            <svg fill="none" stroke="currentColor" strokeLineCap="round" strokeLineJoin="round" stroke-width="2"
                                 className="text-indigo-500 w-12 h-12 mb-3 inline-block"
                                 viewBox="0 0 24 24">
                                <path d="M17 21v-2a4 4 0 00-4-4H5a4 4 0 00-4 4v2"></path>
                                <circle cx="9" cy="7" r="4"></circle>
                                <path d="M23 21v-2a4 4 0 00-3-3.87m-4-12a4 4 0 010 7.75"></path>
                            </svg>
                            <h2 className="title-font font-medium text-3xl text-gray-900"></h2>
                            <p className="leading-relaxed">Utilisateurs</p>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/4 sm:w-1/2 xs:w-full">
                        <div className="border-2 border-gray-200 px-4 py-6 rounded-lg">
                            <svg xmlns="http://www.w3.org/2000/svg" className="text-indigo-500 w-12 h-12 mb-3 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLineCap="round" strokeLineJoin="round" stroke-width="2"
                                      d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2"/>
                            </svg>
                            <h2 className="title-font font-medium text-3xl text-gray-900"></h2>
                            <p className="leading-relaxed">Paris totals</p>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/4 sm:w-1/2 xs:w-full">
                        <div className="border-2 border-gray-200 px-4 py-6 rounded-lg">
                            <svg xmlns="http://www.w3.org/2000/svg" className="text-indigo-500 w-12 h-12 mb-3 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLineCap="round" strokeLineJoin="round" stroke-width="2"
                                      d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2"/>
                            </svg>
                            <h2 className="title-font font-medium text-3xl text-gray-900"></h2>
                            <p className="leading-relaxed">Paris aujourd'hui</p>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/4 sm:w-1/2 xs:w-full">
                        <div className="border-2 border-gray-200 px-4 py-6 rounded-lg">
                            <svg xmlns="http://www.w3.org/2000/svg" className="text-indigo-500 w-12 h-12 mb-3 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLineCap="round" strokeLineJoin="round" stroke-width="2"
                                      d="M14.121 15.536c-1.171 1.952-3.07 1.952-4.242 0-1.172-1.953-1.172-5.119 0-7.072 1.171-1.952 3.07-1.952 4.242 0M8 10.5h4m-4 3h4m9-1.5a9 9 0 11-18 0 9 9 0 0118 0z"/>
                            </svg>
                            <h2 className="title-font font-medium text-3xl text-gray-900"></h2>
                            <p className="leading-relaxed">Balance</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Dashboard
