import {useEffect, useState} from "react";
import Modal from "../Component/Modal";
import TransactionList from "../Component/Transaction/TransactionList";
import TransactionShow from "../Component/Transaction/TransactionShow";
import TransactionForm from "../Component/Transaction/TransactionForm";
import {deleteTransactions, getTransaction,getTransactions} from "../Helper/Transaction.api";

const Transaction = ({theme}) => {

    const [modal, setModal] = useState(false);
    const [modalShow, setModalShow] = useState(false);
    const [transactions, setTransactions] = useState([])
    const [transaction, setTransaction] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [totalPage, setTotalPage] = useState(50)
    const [itemPerPage, setItemPerPage] = useState(10)
    const [pagination, setPagination] = useState({
        currentPage: currentPage,
        totalPage: 20
    })

    async function getDataTransactions() {
        const table = await getTransactions({
            page: currentPage,
            limit: 20
        }, transaction)

        if (table.data){
            setTransactions(table.data.data)
            setPagination({
                currentPage: currentPage,
                totalPage: table.data.total / 20
            })
        }
    }

    useEffect(() => {
        //getDataTransactions()
    }, [currentPage]);

    const openModal = () => {
        setTransaction(null)
        setModal(!modal)
    }

    const openShowModal = async (transaction) => {
        if (transaction !== false) {
            const transactionData = await getTransaction(transaction.id, false)
            setTransaction(transactionData.data)
        } else {
            setTransaction(null)
        }
        setModalShow(!modalShow)
    }

    const edit = async (transaction) => {
        if (transaction !== false) {
            const transactionData = await getTransaction(transaction.id, false)
            setTransaction(transactionData.data)
        } else {
            setTransaction(null)
        }
        setModal(!modal)
    }

    const deleteTransaction = async (id) => {
        const req = await deleteTransactions(id, transaction)
        if (req.status === 204) {
            const data = transactions.filter((transaction) => {
                return transaction.id !== id
            })
            setTransactions(data)
        }
    }


    return (
        <section>
            <TransactionList theme={theme}
                      transactions={transactions}
                      pagination={pagination}
                      changePage={setCurrentPage}
                      openModal={openModal}
                      openShowModal={openShowModal}
                      edit={edit}
                      deleteTransaction={deleteTransaction}
            />
            <Modal
                theme={theme}
                title="ma modal"
                open={modalShow}
                onClose={() => setModalShow(false)}
            >
                <TransactionShow theme={theme} deleteTransaction={deleteTransaction} transaction={transaction}/>
            </Modal>
            <Modal
                theme={theme}
                title={transaction ? "Edit transaction" : "Ajout transaction"}
                open={modal}
                onClose={() => setModal(false)}
            >
                <TransactionForm selectedValue={transaction}/>
            </Modal>
        </section>
    )
}

export default Transaction
