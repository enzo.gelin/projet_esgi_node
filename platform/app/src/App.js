import './App.css';
import Router from "./Router";
import Sidebar from "./Component/Sidebar";
import Header from "./Component/Header";
import {BrowserRouter} from "react-router-dom";
import {store} from "./Store/store";
import {StoreProvider} from "easy-peasy";

function App() {
    return (
        <div className="App flex flex-row dark w-screen">
            <BrowserRouter>
                <StoreProvider className="App" store={store}>
                <Sidebar/>
                <section className={"w-full"}>
                    <Header/>
                    <Router/>
                </section>
                </StoreProvider>
            </BrowserRouter>
        </div>
    );
}

export default App;
