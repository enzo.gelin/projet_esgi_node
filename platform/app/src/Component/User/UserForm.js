import React, {useState} from "react";
import {useStoreState} from "easy-peasy";
import {createUser, patchUser} from "../../Helper/User.api";
import CustomSelect from "../CustomSelect";

export default function UserForm({addUser, selectedValue, closeModal}) {

    const user = useStoreState(state => state.user.user)
    const [files, setFiles] = useState(null)
    const [values, setValues] = useState(
        selectedValue === null
            ? {
                firstname: `a`,
                lastname: `a`,
                email: `a@a${Math.round(Math.random() * 100)}.fr`,
                status: 1,
                company: `a`,
                password: `rootroot`,
                passwordConfirmation: `rootroot`,
                redirect_success: "a",
                redirect_failed: "a",
                is_admin: false,
            }
            : selectedValue
    );

    const select = {
        showList: false,
        value: null,
        defaultSelectText: "Please select an option",
        countryList: [
            {id: 0, name: "Waiting"},
            {id: 1, name: "active"},
            {id: 2, name: "incomplete"},
            {id: 3, name: "refused"}
        ]
    };

    const _onSubmit = async (event) => {
        event.preventDefault();
        values.passwordConfirmation = values.password

        // Vanilla JS approch
        const newData = new FormData();
        Object.keys(values).forEach(key => {
            newData.append(key, values[key])
        })

        if (files !== null){
            newData.append("kbis", files)
        }
        let req;

        if (selectedValue) {
            req = await patchUser(newData,values.id, user)
        } else {
            req = await createUser(newData, user)
        }

        if (req.statusText === "OK") {
            addUser()
            closeModal(false)
        } else {
            /*
             * The request was made and the server responded with a
             * status code that falls out of the range of 2xx
             */
            console.log(req);

        }
    };

    const onChangeFile = event => {
        setFiles(event.target.files[0])
    }

    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value,
        });
    };

    const changeSelect = (value) => {
        values.status = value
        setValues(values)
    }

    const toggleIsAdmin = () => {
        setValues({
            ...values,
            ['is_admin']: !values.is_admin,
        });
    }

    return (
        <section>
            <form onSubmit={_onSubmit}
                  className="flex flex-col w-full p-10 px-8 pt-6 mx-auto my-6 mb-4 bg-white border rounded-lg lg:w-1/2 ">
                <div className="relative pt-4">
                    <label htmlFor="name" className="text-base leading-7 text-blueGray-500">FirstName</label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.firstname} onChange={handleChange} name="firstname" type={"text"}/>
                </div>
                <div className="relative mt-4">
                    <label htmlFor="name" className="text-base leading-7 text-blueGray-500">LastName</label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.lastname} onChange={handleChange} name="lastname" type={"text"}/>
                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Email </label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.email} onChange={handleChange} name="email" type={"email"}/>
                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Company </label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.company} onChange={handleChange} name="company"/>

                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Is Admin </label>
                    <input type={"checkbox"}
                           className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                           defaultChecked={values.is_admin} onChange={toggleIsAdmin} name="is_admin"/>

                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Status </label>
                    <CustomSelect
                        defaultText={select.defaultSelectText}
                        optionsList={select.countryList}
                        changeSelect={changeSelect}
                    />

                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Password </label>
                    <input type="password"
                           className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                           value={values.password} onChange={handleChange} name="password"/>

                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Redirect Success </label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.redirect_success} onChange={handleChange} name="redirect_success"/>

                </div>
                <div className="relative mt-4">
                    <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Redirect Fail </label>
                    <input
                        className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                        value={values.redirect_failed} onChange={handleChange} name="redirect_failed"/>
                </div>
                <section className="flex flex-col w-full h-full p-1 overflow-auto">
                    <header
                        className="flex flex-col items-center justify-center py-12 text-base text-blueGray-500 transition duration-500 ease-in-out transform bg-white border border-dashed rounded-lg focus:border-blue-500 focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2">
                        <input type="file" name={"kbis"} id={"kbis"} onChange={onChangeFile}/>
                    </header>
                </section>
                <div className="flex items-center w-full pt-4">
                    <button
                        className="w-full py-3 text-base text-white transition duration-500 ease-in-out transform bg-blue-600 border-blue-600 rounded-md border-2 border-black focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blue-800 "
                        data-dashlane-rid="33175fb5453a4c0f" data-form-type="other" data-dashlane-label="true"> Valider
                    </button>
                </div>
            </form>
        </section>
    );
}
