import Pagination from "../Pagination";

const UserList = ({users, pagination, changePage, openModal, openShowModal, edit, deleteUser}) => {

    const select = {
        type: [
            {id: 0, name: "Waiting"},
            {id: 1, name: "active"},
            {id: 2, name: "incomplete"},
            {id: 3, name: "refused"}
        ]
    };

    console.log(select.type[0].name)
    return (
        <>
            <div className="w-full sm:px-6">
                <div className="px-4 md:px-10 py-4 md:py-7 bg-gray-100 rounded-tl-lg rounded-tr-lg">
                    <div className="sm:flex items-center justify-between">
                        <p className="text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800">Users</p>
                        <div>
                            <button onClick={openModal}
                                    className="inline-flex sm:ml-3 mt-4 sm:mt-0 items-start justify-start px-6 py-3 bg-indigo-700 hover:bg-indigo-600 focus:outline-none rounded">
                                <p className="text-sm font-medium leading-none text-white">New User</p>
                            </button>
                        </div>
                    </div>
                </div>
                <div className="bg-white shadow px-4 md:px-10 pt-4 md:pt-7 pb-5 overflow-y-auto">
                    <table className="w-full whitespace-nowrap">
                        <thead>
                        <tr className="h-16 w-full text-sm leading-none text-gray-800">
                            <th className="font-normal text-left pl-12">Name</th>
                            <th className="font-normal text-left pl-4">Email</th>
                            <th className="font-normal text-left pl-20">Status</th>
                            <th className="font-normal text-left pl-20">Company</th>
                            <th className="font-normal text-left pl-20">Redirect Success</th>
                            <th className="font-normal text-left pl-20">Redirect Failed</th>
                            <th className="font-normal text-left pl-16">Actions</th>
                        </tr>
                        </thead>
                        <tbody className="w-full">
                        {users && users.map(user => {
                            return <tr className="h-20 text-sm leading-none text-gray-800 bg-white hover:bg-gray-100 border-b border-t border-gray-100" key={user.id}>
                                <td className="pl-4 cursor-pointer">
                                    <div className="flex items-center">
                                        <div className="pl-4">
                                            <p className="font-medium">{user.firstname} {user.lastname}</p>
                                        </div>
                                    </div>
                                </td>
                                <td className="pl-12">
                                    <p className="text-sm font-medium leading-none text-gray-800">{user.email}</p>
                                </td>
                                <td className="pl-20">
                                    <p className="text-xs leading-3 text-gray-600 mt-2">{select.type[user.status].name}</p>
                                </td>
                                <td className="pl-20">
                                    <p className="text-xs leading-3 text-gray-600 mt-2">{user.company}</p>
                                </td>
                                <td className="pl-20">
                                    <p className="text-xs leading-3 text-gray-600 mt-2">{user.redirect_success}</p>
                                </td>
                                <td className="pl-20">
                                    <p className="text-xs leading-3 text-gray-600 mt-2">{user.redirect_failed}</p>
                                </td>
                                <td className="px-7 2xl:px-0 flex flex-row">
                                    <div className="text-xs w-full hover:bg-indigo-700 py-4 px-4 cursor-pointer hover:text-white" onClick={() => openShowModal(user)}>
                                        <p>Show</p>
                                    </div>
                                    <div className="text-xs w-full hover:bg-indigo-700 py-4 px-4 cursor-pointer hover:text-white" onClick={() => edit(user)}>
                                        <p>Edit</p>
                                    </div>
                                    <div className="text-xs w-full hover:bg-indigo-700 py-4 px-4 cursor-pointer hover:text-white" onClick={() => deleteUser(user.id)}>
                                        <p>Delete</p>
                                    </div>
                                </td>
                            </tr>
                        })}

                        </tbody>
                    </table>
                    <Pagination currentPage={pagination.currentPage} totalPage={pagination.totalPage} changePage={changePage}/>
                </div>
            </div>
        </>
    );
}

export default UserList
