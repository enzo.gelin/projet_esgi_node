import Pagination from "../Pagination";

const TransactionList = ({transactions, pagination, changePage, openModal, openShowModal, edit, deleteTransaction}) => {

    return (
        <>
            <div className="w-full sm:px-6">
                <div className="px-4 md:px-10 py-4 md:py-7 bg-gray-100 rounded-tl-lg rounded-tr-lg">
                    <div className="sm:flex items-center justify-between">
                        <p className="text-base sm:text-lg md:text-xl lg:text-2xl font-bold leading-normal text-gray-800">Transactions</p>
                        <div>
                            <button onClick={openModal}
                                    className="inline-flex sm:ml-3 mt-4 sm:mt-0 items-start justify-start px-6 py-3 bg-indigo-700 hover:bg-indigo-600 focus:outline-none rounded">
                                <p className="text-sm font-medium leading-none text-white">New Transaction</p>
                            </button>
                        </div>
                    </div>
                </div>
                <div className="bg-white shadow px-4 md:px-10 pt-4 md:pt-7 pb-5 overflow-y-auto">
                    <table className="w-full whitespace-nowrap">
                        <thead>
                        <tr className="h-16 w-full text-sm leading-none text-gray-800">
                            <th className="font-normal text-left pl-12">Name</th>
                            <th className="font-normal text-left pl-4">Email</th>
                            <th className="font-normal text-left pl-20">address</th>
                            <th className="font-normal text-left pl-20">delivery</th>
                            <th className="font-normal text-left pl-20">price</th>
                            <th className="font-normal text-left pl-16">Actions</th>
                        </tr>
                        </thead>
                        <tbody className="w-full">
                        {transactions && transactions.map(transaction => {
                            return <tr className="h-20 text-sm leading-none text-gray-800 bg-white hover:bg-gray-100 border-b border-t border-gray-100" key={transaction.id}>
                                <td className="pl-4 cursor-pointer">
                                    <div className="flex items-center">
                                        <div className="pl-4">
                                            <p className="font-medium">{transaction.firstname} {transaction.lastname}</p>
                                        </div>
                                    </div>
                                </td>
                                <td className="pl-12">
                                    <p className="text-sm font-medium leading-none text-gray-800">{transaction.email}</p>
                                </td>
                                <td className="pl-20">
                                    <p className="text-xs leading-3 text-gray-600 mt-2">{transaction.address}</p>
                                </td>
                                <td className="pl-20">
                                    <p className="text-xs leading-3 text-gray-600 mt-2">{transaction.delivery}</p>
                                </td>
                                <td className="pl-20">
                                    <p className="text-xs leading-3 text-gray-600 mt-2">{transaction.price} {transaction.currency}</p>
                                </td>
                                <td className="px-7 2xl:px-0 flex flex-row">
                                    <div className="text-xs w-full hover:bg-indigo-700 py-4 px-4 cursor-pointer hover:text-white" onClick={() => openShowModal(transaction.id)}>
                                        <p>Show</p>
                                    </div>
                                    <div className="text-xs w-full hover:bg-indigo-700 py-4 px-4 cursor-pointer hover:text-white" onClick={() => edit(transaction.id)}>
                                        <p>Edit</p>
                                    </div>
                                    <div className="text-xs w-full hover:bg-indigo-700 py-4 px-4 cursor-pointer hover:text-white" onClick={() => deleteTransaction(transaction.id)}>
                                        <p>Delete</p>
                                    </div>
                                </td>
                            </tr>
                        })}

                        </tbody>
                    </table>
                    <Pagination currentPage={pagination.currentPage} totalPage={pagination.totalPage} changePage={changePage}/>
                </div>
            </div>
        </>
    );
}

export default TransactionList
