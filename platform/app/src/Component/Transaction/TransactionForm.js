import React, {useEffect, useState} from "react";
import axios from "axios";
import {createTransaction, patchTransaction} from "../../Helper/Transaction.api";
import {useStoreState} from "easy-peasy";

export default function TransactionForm({onSubmit, selectedValue}) {

    const user = useStoreState(state => state.user.user)
    const [values, setValues] = useState(
        selectedValue === null
            ? {
                user_id: "",
                lastname: "",
                firstname: "",
                email: "",
                address: "",
                delivery: "",
                cart: [],
                price: 0,
                currency: ""
            }
            : selectedValue
    );
    const [cart, setCart] = useState();

    useEffect(() => {
        setValues({...values, cart: cart})
    }, [cart])
    useEffect(() => {
        setCart([])
    }, [])

    const addCart = (e) => {
        e.preventDefault()
        setCart([...cart, {
            "name": "",
            "price": 0
        }])
        console.log(cart)
    }

    const handleChangeCart = (event) => {
        cart[event.target.dataset.key][event.target.name] = event.target.value
        setCart([...cart])
    }

    const deleteCart = (event) => {
        cart.splice(event.target.key, 1)
        setCart([...cart])
    }

    const _onSubmit = async (event) => {
        event.preventDefault();
        // Vanilla JS approch
        const newData = new FormData(event.target);
        Object.keys(values).forEach(key => {
            if (key === "cart"){
                newData.append(key, JSON.stringify(values[key]))
            }else {
                newData.append(key, values[key])
            }
        })
        let req;

        if (selectedValue) {
            req = await patchTransaction(newData, user)
        }else {
            req = await createTransaction(newData, user)
        }

    };

    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value,
        });
    };



    return (
        <section>
        <form onSubmit={_onSubmit}
              className="flex flex-col w-full p-10 px-8 pt-6 mx-auto my-6 mb-4 bg-white border rounded-lg lg:w-1/2 ">
            <div className="relative pt-4">
                <label htmlFor="name" className="text-base leading-7 text-blueGray-500">FirstName</label>
                <input
                    className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                    value={values.firstname} onChange={handleChange} name="firstname" type={"text"}/>
            </div>
            <div className="relative mt-4">
                <label htmlFor="name" className="text-base leading-7 text-blueGray-500">LastName</label>
                <input
                    className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                    value={values.lastname} onChange={handleChange} name="lastname" type={"text"}/>
            </div>
            <div className="relative mt-4">
                <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Email </label>
                <input
                    className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                    value={values.email} onChange={handleChange} name="email" type={"email"}/>
            </div>
            <div className="relative mt-4">
                <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Address </label>
                <input type={"text"}
                    className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                    value={values.address} onChange={handleChange} name="address"/>

            </div>
            <div className="relative mt-4">
                <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Delivery </label>
                <input
                    className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                    value={values.delivery} onChange={handleChange} name="delivery"/>

            </div>
            <div className="relative mt-4">
                <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> price </label>
                <input
                    className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                    value={values.price} onChange={handleChange} name="price"/>

            </div>
            <div className="relative mt-4">
                <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> currency </label>
                <input
                    className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                    value={values.currency} onChange={handleChange} name="currency"/>
            </div>

            <section className="flex flex-col w-full h-full p-1 overflow-auto">
                {values.cart && values.cart.map((item, key) => {
                    return (<section className={"flex flex-row"} key={key}>
                        <section className={"w-1/2"}>
                            <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> Name </label>
                            <input
                                className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                                value={item.name} onChange={handleChangeCart} data-key={key} name="name"/>
                        </section>
                        <section className={"w-1/2"}>
                            <label className="text-base leading-7 text-blueGray-500" htmlFor="description"> currency </label>
                            <input
                                className="w-full px-4 py-2 mt-2 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-blueGray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none border-2 border-gray-200 focus:ring-2 ring-offset-current ring-offset-2"
                                value={item.price} onChange={handleChangeCart} data-key={key} name="price"/>
                        </section>
                        <button onClick={deleteCart} key={key} type={"button"}>Delete</button>
                    </section>)
                })}
                <button onClick={addCart} type={"button"}
                    className="w-full py-3 text-base text-white transition duration-500 ease-in-out
                     transform bg-green-600 border-green-600 rounded-md border-2 border-black focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-green-800 "
                    > Ajouter un objet
                </button>
            </section>
            <div className="flex items-center w-full pt-4">
                <button
                    className="w-full py-3 text-base text-white transition duration-500 ease-in-out transform bg-blue-600 border-blue-600 rounded-md border-2 border-black focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blue-800 "
                    data-dashlane-rid="33175fb5453a4c0f" data-form-type="other" data-dashlane-label="true"> Créer
                </button>
            </div>
        </form>
        </section>
    );
}
