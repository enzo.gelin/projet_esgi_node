import React from "react";
import Button from "../Button";

function TransactionShow({theme,open, user, deleteTransaction}) {

    return (
        <div className={open ? "hidden" : ""}>
            <>
                <div className="w-full">
                    <div className="bg-white px-4 md:px-10 pb-5">
                        <div className="overflow-x-auto">
                            <table className="w-full whitespace-nowrap">
                                <tbody>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Email
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.email}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        First Name
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.firstname}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Last Name
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.lastname}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Status
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.status}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Company
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.company}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Redirect Success
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.redirect_success}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Redirect Failed
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.redirect_failed}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Client ID
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.client_id}</p>
                                    </td>
                                </tr>
                                <tr className="text-sm border-b leading-none  h-16">
                                    <td className="w-1/2">
                                        Secret Id
                                    </td>
                                    <td className="pl-16">
                                        <p>{user.secret_id}</p>
                                    </td>
                                </tr>
                                <tr className="h-16">
                                    <td className="w-1/2" colSpan={2}>
                                        <Button
                                            theme={theme}
                                            title="Delete"
                                            variant={"danger"}
                                            onClick={() => deleteTransaction(user.id)}
                                        />
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </>
        </div>
    );
}

export default TransactionShow;
