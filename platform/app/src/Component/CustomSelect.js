import React, {useState} from "react";

const CustomSelect = ({defaultText, optionsList, changeSelect}) => {

    const [showOptionList, setShowOptionList] = useState(false)
    const [state, setState] = useState({
        defaultSelectText: defaultText,
        optionsList: optionsList
    })

// This method handles the click that happens outside the
    // select text and list area

    const handleClickOutside = e => {
        if (
            !e.target.classList.contains("custom-select-option") &&
            !e.target.classList.contains("selected-text")
        ) {
            setShowOptionList(false);
        }
    };

    document.addEventListener("mousedown", handleClickOutside);

    // This method handles the display of option list
    const handleListDisplay = () => {
        setShowOptionList(!showOptionList);
    };

    // This method handles the setting of name in select text area
    // and list display on selection
    const handleOptionClick = e => {
        setState({
            defaultSelectText: e.target.getAttribute("data-name"),
            optionList: state.optionsList
        });
        changeSelect(e.target.getAttribute("data-key"))
        setShowOptionList(false)
    };

    return (
        <div className="custom-select-container flex flex-col mb-6">
            <div className="border border-gray-200 dark:border-gray-700 shadow-sm rounded flex">
                <section className="pl-3 py-3 w-full text-sm focus:outline-none border border-transparent focus:border-indigo-700
                 bg-transparent rounded placeholder-gray-500 text-gray-500 dark:text-gray-400">
                    <label
                        className={(showOptionList ? "selected-text active" : "selected-text") + " pb-2 text-sm font-bold"}
                        onClick={handleListDisplay}
                        htmlFor="City">
                        {state.defaultSelectText}
                    </label>
                    {showOptionList && (
                        <ul className="select-options">
                            {optionsList.map(option => {
                                return (
                                    <li
                                        className="custom-select-option"
                                        data-name={option.name}
                                        data-key={option.id}
                                        key={option.id}
                                        onClick={handleOptionClick}
                                    >
                                        {option.name}
                                    </li>
                                );
                            })}
                        </ul>
                    )}
                </section>
                <div className="px-4 flex items-center border-l border-gray-300 dark:border-gray-700 flex-col justify-center text-gray-500 dark:text-gray-400">
                    <svg onClick={handleListDisplay} xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-chevron-up" width={16} height={16}
                         viewBox="0 0 24 24"
                         strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z"/>
                        <polyline points="6 15 12 9 18 15"/>
                    </svg>
                    <svg onClick={handleListDisplay} xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-chevron-down" width={16} height={16}
                         viewBox="0 0 24 24"
                         strokeWidth="1.5" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z"/>
                        <polyline points="6 9 12 15 18 9"/>
                    </svg>
                </div>
            </div>
        </div>
    );
}

export default CustomSelect;
