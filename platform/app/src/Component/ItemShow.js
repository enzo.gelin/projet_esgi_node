import React from "react";
import Button from "./Button";

function ItemShow({ theme, deleteUser }) {
    return (
        <div>
            Item #1
            <Button
                theme={theme}
                title="Delete"
                onClick={() => alert("item deleted")}
            />
        </div>
    );
}

export default ItemShow;
