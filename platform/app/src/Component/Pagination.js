import {faArrowLeft, faArrowRight} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import React, {useEffect, useState} from "react";

const Pagination = ({changePage, currentPage, totalPage}) => {

    const [list, setList] = useState([])
    let end = 1
    let start = 1
    const renderPagination = () =>
    {
        const buttons = [];
        let start = currentPage - 5 > 0 ? currentPage - 5 : 1
        let end = currentPage + 5 < totalPage ? currentPage + 5 : totalPage

        for(let i=start; i <= end; i++){
            buttons.push(
                <li className={"grid place-items-center"} key={i} onClick={() => changePage(i)}>
                    <span className={"cursor-pointer text-sm leading-none flex rounded-full w-8 h-8 mx-1 p-0 items-center justify-center focus:bg-gray-500 focus:bg-opacity-20 active:bg-gray-500 active:bg-opacity-40 transition-all duration-300 " + (currentPage === i ? "bg-blue-400 text-white" : "bg-transparent text-gray-700 hover:bg-gray-500 hover:bg-opacity-20")}>  { i } </span>
                </li>
            )
        }

        setList(buttons)
    }

    const navPagination = (type) => {
        if (type === "add"){
            if(currentPage < end){
                changePage(++currentPage)
            }
        }
        else if (type === "remove"){
            if(currentPage > start){
                changePage(--currentPage)
            }
        }
    }

    useEffect(() => {
        renderPagination()
    }, [currentPage, totalPage]);

    return (
        <div className="flex justify-center">
            <div className="py-2">
                <div className="block">
                    <ul className="flex pl-0 rounded list-none">
                        <li onClick={() => navPagination("remove")} className="grid place-items-center text-sm leading-none flex rounded-full w-8 h-8 mx-1 p-0 items-center
                            justify-center bg-transparent text-gray-700 hover:bg-gray-500 hover:bg-opacity-20 focus:bg-gray-500
                            focus:bg-opacity-20 active:bg-gray-500 active:bg-opacity-40 transition-all duration-300">
                            <span><FontAwesomeIcon icon={faArrowLeft}/></span>
                        </li>
                        {list && list.map(button => {
                            return button
                        })}
                        <li onClick={() => navPagination("add")} className="grid place-items-center text-sm leading-none flex rounded-full w-8 h-8 mx-1 p-0 items-center
                            justify-center bg-transparent text-gray-700 hover:bg-gray-500 hover:bg-opacity-20 focus:bg-gray-500
                            focus:bg-opacity-20 active:bg-gray-500 active:bg-opacity-40 transition-all duration-300">
                            <span><FontAwesomeIcon icon={faArrowRight}/></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Pagination
