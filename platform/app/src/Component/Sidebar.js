import "../styles/sidebar.css"
import {useStoreState} from "easy-peasy";
import {Link} from "react-router-dom";

const Sidebar = () => {

    const user = useStoreState(state => state.user.user)

    if (user === null || user.access === ""){
        return <></>
    }

    return (
        <div className="sidebar">
            <div className="sidebar__container">
                {user && (
                    <section>
                        <div className="flex flex-col items-center flex-shrink-0 bg-gray-50">
                            <button className="sidebar__close" type="button" aria-label="Close" aria-hidden="true">
                                <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-circle-x"
                                     width="24" height="24" viewBox="0 0 24 24" stroke="currentColor"
                                     fill="none">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                    <circle cx="12" cy="12" r="9"/>
                                    <path d="M10 10l4 4m0 -4l-4 4"/>
                                </svg>
                            </button>
                        </div>
                        <div className="sidebar__user">
                            <img alt="testimonial" className="sidebar__user-avatar" src="https://dummyimage.com/302x302/F3F4F7/8693ac"/>
                            <h2 className="sidebar__user-name">{user.email}</h2>
                            <p className="text-gray-500">Administrator</p>
                        </div>
                        <nav className="flex-grow pb-4 pr-4 h-full md:block md:pb-0 md:overflow-y-auto">
                            <ul>
                                <li>
                                    <Link className="sidebar__link" to="/dashboard">
                                        Dashboard
                                    </Link>
                                </li>
                                <li>
                                    <Link className="sidebar__link"
                                          to="/dashboard/user">User</Link>
                                </li>
                                <li>
                                    <Link className="sidebar__link"
                                          to="/dashboard/transaction">Transactions</Link>
                                </li>
                            </ul>
                        </nav>
                    </section>
                )}
            </div>
        </div>
    )
}

export default Sidebar
