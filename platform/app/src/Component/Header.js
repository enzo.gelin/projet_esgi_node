const Header = () => {

    return (
        <div className="w-full items-center">
            <div className="text-gray-700 transition duration-500 ease-in-out transform bg-white border rounded-lg">
                <div className="flex flex-row flex-wrap p-5 mx-auto md:items-center md:flex-row">
                    <a href=""
                       className="block p-2 mr-4 text-base text-gray-500 transition duration-500 ease-in-out transform rounded-full
                       focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 lg:ml-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" className="mx-auto icon icon-tabler icon-tabler-logout" width="24" height="24" viewBox="0 0 24 24"
                             stroke="currentColor" fill="none">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                            <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2"/>
                            <path d="M7 12h14l-3 -3m0 6l3 -3"/>
                        </svg>
                    </a>
                    <div className="relative ml-3">
                        <div>
                            <a href=""
                               className="flex text-sm bg-gray-800 rounded-full focus:outline-none focus:ring-2
                               focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                               id="user-menu" aria-haspopup="true">
                                <span className="sr-only">Open user menu</span>
                                <i className="fa fa-user"/>
                            </a>
                        </div>
                        <div
                            className="absolute right-0 hidden w-48 px-4 py-2 mt-2 transition duration-500
                            ease-in-out origin-top-right transform bg-white border rounded-lg shadow-lg ring-1 ring-black ring-opacity-5"
                            role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
                            <a href=""
                               className="block px-4 py-1 my-1 text-sm text-gray-500 transition duration-500
                               ease-in-out transform rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blueGray-100 "
                               role="menuitem">Your Profile</a>
                            <a href="#"
                               className="block px-4 py-1 my-1 text-sm text-gray-500 transition duration-500
                               ease-in-out transform rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blueGray-100 "
                               role="menuitem">Settings</a>
                            <a href=""
                               className="block px-4 py-1 my-1 text-sm text-gray-500 transition duration-500
                               ease-in-out transform rounded-md focus:shadow-outline focus:outline-none focus:ring-2 ring-offset-current ring-offset-2 hover:bg-blueGray-100 "
                               role="menuitem">Sign out</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header
