import React from "react";
import "../styles/modal.css";

function Modal({ theme, title, open, onClose, children }) {
    const styleModal = {
        backgroundColor: theme === "dark" ? "black" : "white",
        borderColor: theme === "dark" ? "white" : "black",
        color: theme === "dark" ? "white" : "black",
    };
    return (
        open && (
            <div className="container items-center px-5 py-12 lg:px-20">
                <div className="bg-white modal w-1/2 px-5 mx-auto border rounded-lg shadow-xl lg:px-0 max-h-screen overflow-y-scroll lg:w-1/2" aria-hidden="false" aria-describedby="modalDescription"
                     role="dialog">
                    <div className="flex items-center justify-end px-6 py-4 ">
                        <button onClick={onClose} className="p-1 transition-colors duration-200 transform rounded-md hover:bg-opacity-25 hover:bg-blueGray-600 focus:outline-none" type="button"
                                aria-label="Close" aria-hidden="true">
                            <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-circle-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5"
                                 stroke="currentColor" fill="none" strokeLineCap="round" strokeLineJoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <circle cx="12" cy="12" r="9"></circle>
                                <path d="M10 10l4 4m0 -4l-4 4"></path>
                            </svg>
                        </button>
                    </div>
                    <div className="modal-content flex flex-col w-full mb-8 text-center">
                        <h1 className="mx-auto mb-8 text-2xl font-semibold leading-none tracking-tighter text-black lg:w-1/2 sm:text-4xl title-font">{title}</h1>
                        {children}
                    </div>
                </div>
            </div>
        )
    );
}

export default Modal;
