import React from "react";

function Button({
                    variant = "default",
                    title,
                    size = 20,
                    theme,
                    ...restProps
                }) {
    const style = {textTransform: "uppercase"};
    let className = "";

    if (["rounded", "icon"].includes(variant)) {
        style.borderRadius = "50%";
    }

    if (variant === "icon") {
        style.width = size;
        style.height = style.width;
        style.maxHeight = style.width;
        style.maxWith = style.width;
        style.minHeight = style.width;
        style.minWith = style.width;
    }else if(variant === "danger"){
        className = "bg-red-500 rounded text-white w-full py-2 px-2"
    }

    style.color = theme === "dark" ? "white" : "black";
    style.backgroundColor = theme !== "dark" ? "white" : "black";

    return (
        <button className={className} {...restProps}>
            {title}
        </button>
    );
}

export default Button;
